public class MissingLetter {

    public static char findMissingLetter(char[] array) {

        for (int i = 0; i < array.length - 1; i++) {
            if (((int) array[i + 1] - (int) array[i]) > 1) return (char) ((int) array[i] + 1);
        }
        return ' ';
    }
}
