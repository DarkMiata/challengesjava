import java.util.ArrayList;

public class Metro {

    public static int countPassengers(ArrayList<int[]> stops) {
        int sumIn = 0, sumOut = 0;

        for (int[] stop : stops) {
            sumIn += stop[0];
            sumOut += stop[1];
        }

        return sumIn - sumOut;
    }

    public static int soluceCountPassengers(ArrayList<int[]> stops) {
        return stops.stream()
                    .mapToInt(x -> x[0] - x[1])
                    .sum();
    }
}
