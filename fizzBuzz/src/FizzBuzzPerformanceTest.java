import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

@DisplayName("Performance Tests")
public class FizzBuzzPerformanceTest {

    private FizzBuzz fizzBuzz;

    @BeforeEach
    void setUp() {
        fizzBuzz = new FizzBuzz();
    }

    @Test
    @RepeatedTest(10)
    @DisplayName("Performance generate x 5.000.000")
    public void performanceGenerate5000000() {
        fizzBuzz.performance(5000000, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 15});
    }
}
