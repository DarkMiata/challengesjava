import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FizzBuzzTest {

    private FizzBuzz fizzBuzz;

    @BeforeEach
    void setUp() {
        fizzBuzz = new FizzBuzz();
    }

    @AfterEach
    void tearDown() {
    }

    @Nested
    @DisplayName("Generate with int")
    class TestInt {

        @Test
        @DisplayName("generate(1) => \"1\"")
        public void shouldGenerate1If1() {
            assertEquals("1", fizzBuzz.generate(1));
        }


        @Test
        @DisplayName("generate(2) => \"2\"")
        public void shouldGenerate2If2() {
            assertEquals("2", fizzBuzz.generate(2));
        }

        @Test
        @DisplayName("generate(3) => \"Buzz\"")
        public void shouldGenerateBuzzIs3() {
            assertEquals("Buzz", fizzBuzz.generate(3));
        }

        @Test
        @DisplayName("generate(5) => \"Fizz\"")
        public void shouldGenerateFizzIs5() {
            assertEquals("Fizz", fizzBuzz.generate(5));
        }

        @Test
        @DisplayName("generate(15) => \"FizzBuzz\"")
        public void shouldGenerateFizzBuzzIs15() {
            assertEquals("FizzBuzz", fizzBuzz.generate(15));
        }
    }

    @Nested
    @DisplayName("Generate with array")
    class TestArray {

        @Test
        @DisplayName("Generate([1, 2]) => \"12\"")
        public void shouldGenerate12With12() {
            assertEquals("12", fizzBuzz.generate(new int[]{1, 2}));
        }

        @Test
        @DisplayName("Generate([1, 3]) => \"1Buzz\"")
        public void shouldGenerate1BuzzWith13() {
            assertEquals("1Buzz", fizzBuzz.generate(new int[]{1, 3}));
        }

        @Test
        @DisplayName("Generate([5, 2]) => \"Fizz2\"")
        public void shouldGenerateFizz2With52() {
            assertEquals("Fizz2", fizzBuzz.generate(new int[]{5, 2}));
        }

        @Test
        @DisplayName("Generate([15, 4]) => \"FizzBuzz4\"")
        public void shouldGenerateFizzBuzz4With15_4() {
            assertEquals("FizzBuzz4", fizzBuzz.generate(new int[]{15, 4}));
        }

        @Test
        @DisplayName("Generate([1,2,3,4,5,6,7,8,9,15] => \"12Buzz4FizzBuzz78BuzzFizzBuzz\"")
        public void shouldGenerate12Buzz4FizzBuzz78BuzzFizzBuzzWithLong() {
            assertEquals("12Buzz4FizzBuzz78BuzzFizzBuzz", fizzBuzz.generate(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 15}));
        }
    }
}