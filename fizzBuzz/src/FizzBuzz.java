public class FizzBuzz {
    public String generate(int i) {
        return evaluateNumber(i);
    }

    // Perf 10.7s / 5e6
    private String evaluateNumber(int i) {
        if (i % 15 == 0) return "FizzBuzz";
        if (i % 3 == 0) return "Buzz";
        if (i % 5 == 0) return "Fizz";

        return String.valueOf(i);
    }

    // Perf 13.93s / 5e6
//    private String evaluateNumber(int i) {
//        String returnTxt = "";
////        if (i % 15 == 0) return "FizzBuzz";
//        if (i % 5 == 0) returnTxt += "Fizz";
//        if (i % 3 == 0) returnTxt += "Buzz";
//        if (returnTxt.toString() == "") {
//            return String.valueOf(i);
//        } else return returnTxt;
//    }

    public String generate(int[] array) {
        String incTxt    = "";
        String concatTxt = "";

        for (int number : array) {
            incTxt = evaluateNumber(number);
            concatTxt += incTxt;
        }

        return concatTxt;
    }

    public void performance(int numberIteractions, int[] array) {
        for (int i = 0; i < numberIteractions; i++) {
            String fizzBuzz = generate(array);
        }
    }
}
