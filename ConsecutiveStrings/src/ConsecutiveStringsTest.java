import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConsecutiveStringsTest {

    @Test
    @DisplayName("k==0 => \"\"")
    void nIsZero() {
        assertEquals("", ConsecutiveStrings.longest_consec(new String[]{""}, 0));
    }

    @Test
    @DisplayName("lenght < k1 \"\"")
    void shouldNullWhenLenghtInfK1() {
        assertEquals("", ConsecutiveStrings.longest_consec(new String[]{""}, 2));
    }

    @Test
    @DisplayName("lenght < k2 \"\"")
    void shouldNullWhenLenghtInfK2() {
        assertEquals("", ConsecutiveStrings.longest_consec(new String[]{"test", "test2"}, 3));
    }

    @Test
    @DisplayName("array,2 => abigailtheta")
    void testReturnAbigailtheta() {
        assertEquals("abigailtheta", ConsecutiveStrings
                             .longest_consec(new String[]{"zone", "abigail", "theta", "form", "libe", "zas", "theta", "abigail"}, 2)
                    );
    }

    @Test
    @DisplayName("array,3 => abigailthegfgjtajform")
    void testReturnAbigailthegfgjtajform() {
        assertEquals("abigailthegfgjtajform", ConsecutiveStrings
                             .longest_consec(new String[]{"zone", "abigail", "thegfgjta", "jform", "libe", "zas",
                                                          "theta", "abigail"}, 3)
                    );
    }
}