class ConsecutiveStrings {

    public static String longest_consec(String[] strings, int k) {
        int length = strings.length;

        if (length < k | k <= 0) return "";

        String maxString = "";
        int    maxLength = 0;

        for (int i = 0; i < length - k + 1; i++) {
            int           sumCountString = 0;
            StringBuilder concat         = new StringBuilder();

            for (int j = 0; j < k; j++) {
                int    indexCurrentString = i + j;
                String currentString      = strings[indexCurrentString];
                sumCountString += currentString.length();
                concat.append(currentString);
            }

            if (maxLength < sumCountString) {
                maxLength = sumCountString;
                maxString = concat.toString();
            }
        }

        return maxString;
    }
}
