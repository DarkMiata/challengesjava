public class PigLatin {

    public static String pigIt(String str) {
        String[] words  = str.split("\\W+"); // Separate words in array
        String[] poncts = str.split("\\w+"); // Separate ponctuations in array

        for (int i = 0; i < words.length; i++) {
            String word      = words[i];
            int    length    = word.length();
            char   beginChar = word.charAt(0); // first char of word
            String medString = (length > 1) ? word.substring(1, length) : "";
            words[i] = medString + beginChar + "ay"; // concat
        }

        String pigLatin = "";
        int    incPonct = 1;

        for (String word : words) { // reconstruct sentence
            pigLatin += word + ((incPonct < poncts.length) ? poncts[incPonct++] : "");
        }

        return pigLatin;
    }
}
