public class Dubstep {

    public static String SongDecoder(String song) {
        return song.replaceAll("WUB", " ").replaceAll("\\s{2,}", " ").trim();
    }

    public static String soluceSongDecoder (String song)
    {
        return song.replaceAll("(WUB)+", " ").trim();
    }
}
