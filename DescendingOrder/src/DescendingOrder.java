import java.util.Comparator;
import java.util.stream.Collectors;

import static java.lang.Integer.valueOf;

class DescendingOrder {

    public static int sortDesc(final int num) {
        String sortedNumbers = String.valueOf(num).codePoints()
                                     .mapToObj(c -> String.valueOf((char) c))
                                     .sorted(Comparator.reverseOrder())
                                     .collect(Collectors.joining());

        return valueOf(sortedNumbers);
    }
}
