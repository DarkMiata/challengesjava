import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DescendingOrderTest {

    @Test
    @DisplayName("0 => 0")
    public void test_01() {
        assertEquals(0, DescendingOrder.sortDesc(0));
    }

    @Test
    @DisplayName("15 => 51")
    public void test_02() {
        assertEquals(51, DescendingOrder.sortDesc(15));
    }


    @Test
    @DisplayName("123456789 => 987654321")
    public void test_03() {
        assertEquals(987654321, DescendingOrder.sortDesc(123456789));
    }
}