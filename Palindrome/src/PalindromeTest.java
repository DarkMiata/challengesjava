import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class PalindromeTest {

    private Palindrome pal;

    @BeforeEach
    void setUp() {
        pal = new Palindrome();
    }

    @AfterEach
    void tearDown() {
    }

    @Nested
    @DisplayName("IsPalindrome tests")
    class IsPalindrome {
        @Test
        @DisplayName("\"\" => false")
        public void shouldNullIsFalse() {
            assertFalse(pal.isPalindrome(""));
        }

        @Test
        @DisplayName("radar, true")
        public void shouldRadarIsTrue() {
            assertTrue(pal.isPalindrome("radar"));
        }

        @Test
        @DisplayName("ridar, false")
        public void shouldRidarIsFalse() {
            assertFalse(pal.isPalindrome("ridar"));
        }

        @Test
        @DisplayName("racecar, true")
        public void shouldRacecarIsTrue() {
            assertTrue(pal.isPalindrome("racecar"));
        }


        @Test
        @DisplayName("\"Was it a car, or a cat I saw ?\", true")
        public void shouldWasItACarOrACatISawIsTrue() {
            assertTrue(pal.isPalindrome("Was it a car, or a cat I saw ?"));
        }

        @Test
        @DisplayName("\"nixon\", true")
        public void nixon() {
            assertTrue(pal.isPalindrome("No 'x' in Nixon"));
        }

        @ParameterizedTest
        @ValueSource(strings = {
                "racecar", "radar", "able was I ere I saw elba",
                "A man, a plan, a canal, Panama!", "Was it a car or a cat I saw?",
                "No 'x' in Nixon", "abcba", "abcddcba"
        })
        void palindromes(String candidate) {
            assertTrue(pal.isPalindrome(candidate));
        }

        @ParameterizedTest
        @ValueSource(strings = {
                "fglkgj", "abcdef", "iuynirezbc"
        })
        void notPalindromes(String candidate) {
            assertFalse(pal.isPalindrome(candidate));
        }
    }

    @Nested
    @DisplayName("cleanString test")
    class CleaningString {
        @Test
        @DisplayName("\"Was it a car, or a cat I saw ?\" => \"wasitacaroracatisaw\"")
        public void shouldWasItACarOrACatISawIswasitacaroracatisaw() {
            assertEquals(
                    "wasitacaroracatisaw",
                    pal.cleanString("Was it a car, or a cat I saw ?")
                        );
        }

        @Test
        @DisplayName("\"No 'x' in Nixon\" => \"noxinnixon\"")
        public void shouldNoXInNixonIsNoxinnixon() {
            assertEquals("noxinnixon", pal.cleanString("No 'x' in Nixon"));
        }
    }
}