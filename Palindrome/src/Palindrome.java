public class Palindrome {

    public boolean isPalindrome(String txt) {

        txt = cleanString(txt);
        int txtLength = txt.length();

        if (txtLength == 0) return false;

        int indexStart = 0;
        int indexEnd   = txtLength - 1;
        while ((indexEnd - indexStart) >= 1) {
            char charStart = txt.charAt(indexStart++);
            char charEnd   = txt.charAt(indexEnd--);

            if (charStart != charEnd) return false;

        }

        return true;
    }

    public String cleanString(String txt) {
        txt = txt.replaceAll("[^a-zA-Z]", "");
        txt = txt.toLowerCase();

        return txt;
    }
}