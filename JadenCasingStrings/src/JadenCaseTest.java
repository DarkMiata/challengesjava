import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JadenCaseTest {

    JadenCase jadenCase = new JadenCase();

    @Test
    @DisplayName("most trees are blue => Most Trees Are Blue")
    public void test() {
        assertEquals("Most Trees Are Blue", jadenCase.toJadenCase("most trees are blue"));
    }

    @Test
    @DisplayName("Must return null when the arg is null")
    public void testNullArg() {
        assertNull(jadenCase.toJadenCase(null));
    }

    @Test
    @DisplayName("Must return null when the arg is empty string")
    public void testEmptyArg() {
        assertNull(jadenCase.toJadenCase(""));
    }
}