import java.util.ArrayList;

public class JadenCase {

    public String toJadenCase(String phrase) {

        if (phrase == "" || phrase == null)
            return null;

        ArrayList<String> jadenList = new ArrayList();

        for (String word : phrase.split(" ")) {
            jadenList.add(word.substring(0, 1).toUpperCase() + word.substring(1));

        }

        return String.join(" ", jadenList);
    }
}
