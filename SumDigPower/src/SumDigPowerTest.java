import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class SumDigPowerTest {

    private static void testing(long a, long b, long[] res) {
        assertEquals(
                Arrays.toString(res),
                Arrays.toString(SumDigPower.sumDigPow(a, b).toArray())
                    );
    }

    @Test
    public void test() {
        System.out.println("Basic Tests");
        testing(1, 10, new long[]{1, 2, 3, 4, 5, 6, 7, 8, 9});
        testing(1, 100, new long[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 89});
        testing(10, 100, new long[]{89});
        testing(90, 100, new long[]{});
        testing(90, 150, new long[]{135});
        testing(50, 150, new long[]{89, 135});
        testing(10, 150, new long[]{89, 135});
    }

    @Nested
    @DisplayName("Tests isDigPow")
    class IsDigPow {

        @Test
        @DisplayName("test 1 => true")
        public void should1IsTrue() {
            assertTrue(SumDigPower.isDigPow(1));
        }

        @Test
        @DisplayName("test 89 => true")
        public void should89IsTrue() {
            assertTrue(SumDigPower.isDigPow(89));
        }

        @Test
        @DisplayName("test 90 => false")
        public void should90IsFalse() {
            assertFalse(SumDigPower.isDigPow(90));
        }
    }
}
