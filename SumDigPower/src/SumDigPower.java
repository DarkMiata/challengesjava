import java.util.ArrayList;
import java.util.List;

public class SumDigPower {

    public static List<Long> sumDigPow(long a, long b) {
        List<Long> list = new ArrayList<>();

        for (long i = a; i < b; i++) {
            if (isDigPow(i)) {
                list.add(i);
            }
        }

        return list;
    }

    public static Boolean isDigPow(long a) {
        double sumPow = 0, exp = 1;

        for (char x : Long.toString(a).toCharArray()) {
            sumPow += Math.pow((double) (x - 48), exp++);
        }

        if ((double) a == sumPow) return true;

        return false;
    }
}
