import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class TwoToOne {

    public static String longest(String s1, String s2) {
        ArrayList<Character>    listChars          = new ArrayList<>();
        Map<Character, Boolean> charDuplicateState = new HashMap<>();

        String concatS1S2 = s1 + s2;

        // Scan and add to List if is not duplicate
        for (int i = 0; i < concatS1S2.length(); i++) {
            Character charIndex = concatS1S2.charAt(i);

            if (!charDuplicateState.containsKey(charIndex)) {
                charDuplicateState.put(charIndex, true);
                listChars.add(charIndex);
            }
        }

        Collections.sort(listChars);

        // Concat ArrayList<Character> to String
        return listChars.toString().replaceAll("[,\\s\\[\\]]", "");
    }
}
