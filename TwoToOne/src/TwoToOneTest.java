import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TwoToOneTest {

    @Test
    @DisplayName("null => null")
    public void whenNull() {
        assertEquals("", TwoToOne.longest("", ""));
    }

    @Test
    @DisplayName("ab, cd => abcd")
    public void whenAbcd() {
        assertEquals("abcd", TwoToOne.longest("ab", "cd"));
    }

    @Test
    @DisplayName("aabbbb, ccd => abcd")
    public void whenAabbbbccd() {
        assertEquals("abcd", TwoToOne.longest("aabbbb", "ccd"));
    }

    @Test
    @DisplayName("aretheyhere, yestheyarehere => aehrsty")
    public void test1() {
        assertEquals("aehrsty", TwoToOne.longest("aretheyhere", "yestheyarehere"));
    }

    @Test
    @DisplayName("loopingisfunbutdangerous, lessdangerousthancoding => abcdefghilnoprstu")
    public void test2() {
        assertEquals("abcdefghilnoprstu", TwoToOne.longest("loopingisfunbutdangerous", "lessdangerousthancoding"));
    }

    @Test
    @DisplayName("inmanylanguages, theresapairoffunctions => acefghilmnoprstuy")
    public void test3() {
        assertEquals("acefghilmnoprstuy", TwoToOne.longest("inmanylanguages", "theresapairoffunctions"));
    }
}